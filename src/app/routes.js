import getEvaluationForm from './model/schemas/getEvaluationForm'
import insertEvaluationForm from './model/schemas/insertEvaluationForm'
import updateEvaluationForm from './model/schemas/updateEvaluationForm'
import getMember from './model/schemas/getMember'
import insertMember from './model/schemas/insertMember'
import getEvaluationResultByCid from './model/schemas/getEvaluationResultByCid'

const checkEvaluationForm = (connection,result, body) => {
    if(result.assessor === undefined && body.assessor === undefined ){
        insertEvaluationForm(connection, body)
    }else{
        updateEvaluationForm(connection, body)
    }
}

const checkMember = (connection,result, body) => {
    if(result.length === 0){
        insertMember(connection, body)
    }
}

const evaluationResultByCid = (result, res) => {
    res.send(result)
}

const route = (server, connection) => { 
    server.get('/evaluation-result/:cid/span-of-age/:soa', (req, res) => {
        getEvaluationResultByCid(connection, evaluationResultByCid, req, res)
    })
    server.get('/users/:userId', function (req, res) {
        res.send(req.params)
      })
    server.post('/insert-evaluation-form', (req, res) => {
        getEvaluationForm(connection, checkEvaluationForm, req.body)
        res.status(200).end()
    })
    server.post('/insert-member', (req, res) => {
        getMember(connection, checkMember, req.body)
        res.status(200).end()
    })
}

export default route