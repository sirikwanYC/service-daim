import mysql from 'mysql'
import '@babel/polyfill'
import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import { nodePort, database } from './config'
import route from './app/routes'

const connection = mysql.createConnection({
    host     : database.host,
    user     : database.user,
    password : database.password,
    database : database.databaseName,
    insecureAuth : true
  })


connection.connect(function(err) {
    if (err) throw err
    console.log("Connected!")
  })

  const server = express()

  server.use(
    cors(),
    bodyParser.json(),
    bodyParser.urlencoded({
      extended: true,
    }),
  )

route(server, connection)

server.listen(nodePort, () => console.log(`running server now! ${nodePort}`))

export default server
